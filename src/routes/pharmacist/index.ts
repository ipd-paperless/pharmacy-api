import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./admit'), { prefix: '/pharmacist/admit' });
  fastify.register(require('./doctor-order'), { prefix: '/pharmacist/doctor-order' });
  // fastify.register(require('./medicine-administration-record'), { prefix: '/pharmacist/medicine-administration-record' });
  // fastify.register(require('./dispensing'), { prefix: '/pharmacist/dispensing' });

  done();

} 
