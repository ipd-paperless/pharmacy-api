import { AxiosResponse } from "axios";
import * as jwt from 'jsonwebtoken';
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _, { identity } from 'lodash';
import { DateTime } from 'luxon';

import { UUID } from "crypto";
import { AdmitModel } from '../../models/admit';
import { PatientModel } from '../../models/patient';
import { DepartmentModel } from "../../models/department";
import { WardModel } from "../../models/ward";
import { BedModel } from "../../models/bed";
import { InsuranceModel } from "../../models/insurance";
import { DoctorModel } from "../../models/doctor";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const admitModel = new AdmitModel();
  const patientModel = new PatientModel();
  const departmentModel = new DepartmentModel();
  const wardModel = new WardModel();
  const bedModel = new BedModel();
  const insuranceModel = new InsuranceModel();
  const doctorModel = new DoctorModel();

  
  fastify.get('/', {
    // preHandler: [fastify.guard.role('ADMIN')],
    // schema: listSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const userID: any = request.user.sub;

      const query: any = request.query;
      const { limit, offset, search } = query;
      const _limit = limit || 20;
      const _offset = offset || 0;

      var data: any = {};
      var ans: any[] = [];
      var departmentIDs: UUID[] = [];
      var wardIDs: UUID[] = [];
      var bedIDs: UUID[] = [];
      var insuranceIDs: UUID[] = [];
      var doctorIDs: UUID[] = [];

      var admits: any = await admitModel.getAll(db, search, _limit, _offset);
      const affectedRow: any = await admitModel.getTotal(db, search);

      admits.filter((admit:any) => {
        if(!_.isNull(admit.an)) ans.push(admit.an);
        if(!_.isNull(admit.department_id)) departmentIDs.push(admit.department_id)
        if(!_.isNull(admit.ward_id)) wardIDs.push(admit.ward_id)
        if(!_.isNull(admit.bed_id)) bedIDs.push(admit.bed_id)
        if(!_.isNull(admit.insurance_id)) insuranceIDs.push(admit.insurance_id)
        if(!_.isNull(admit.doctor_id)) doctorIDs.push(admit.doctor_id)
      })

      var patients: any = await patientModel.getByANs(db, ans);
      var departments: any = await departmentModel.getByIDs(db, departmentIDs);
      var wards: any = await wardModel.getByIDs(db, wardIDs);
      var beds: any = await bedModel.getByIDs(db, bedIDs);
      var insurances: any = await insuranceModel.getByIDs(db, insuranceIDs);
      var doctors: any = await doctorModel.getByUserID(db, doctorIDs);

      data = admits.map((admit: any) => {
        const patient = patients.filter((patient: any) => {
          return patient.an == admit.an
        })[0]

        const department = departments.filter((department: any) => {
          return department.id == admit.department_id
        })[0]

        const ward = wards.filter((ward: any) => {
          return ward.id == admit.ward_id
        })[0]

        const bed = beds.filter((bed: any) => {
          return bed.id == admit.bed_id
        })[0]

        const insurance = insurances.filter((insurance: any) => {
          return insurance.id == admit.insurance_id
        })[0]

        const doctor = doctors.filter((doctor: any) => {
          return doctor.user_id == admit.doctor_id
        })[0]

        return {
          ...admit,
          patient,
          department,
          ward,
          bed,
          insurance,
          doctor
        }
      })

      return reply.status(StatusCodes.OK).send({
        ok: true,
        data,
        total: Number(affectedRow.total)
      });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          message: 'error',
          error
        });
    }
  });

  done();
} 
