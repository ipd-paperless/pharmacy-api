import { AxiosResponse } from "axios";
import * as jwt from 'jsonwebtoken';
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _, { identity } from 'lodash';
import { DateTime } from 'luxon';

import { UUID } from "crypto";
import { AdmitModel } from '../../models/admit';
import { PatientModel } from '../../models/patient';
import { DepartmentModel } from "../../models/department";
import { WardModel } from "../../models/ward";
import { BedModel } from "../../models/bed";
import { InsuranceModel } from "../../models/insurance";
import { DoctorModel } from "../../models/doctor";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const admitModel = new AdmitModel();
  const patientModel = new PatientModel();
  const departmentModel = new DepartmentModel();
  const wardModel = new WardModel();
  const bedModel = new BedModel();
  const insuranceModel = new InsuranceModel();
  const doctorModel = new DoctorModel();

  // get doctor order by date
  // /pharmacist/doctor-order/:doctor_order_date/doctor-orders
  fastify.get('/:doctor_order_date/doctor-orders', {
    // preHandler: [fastify.guard.role('ADMIN')],
    // schema: listSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const userID: any = request.user.sub;

      const query: any = request.query;
      const { limit, offset, search } = query;
      const _limit = limit || 20;
      const _offset = offset || 0;

      var data = {};

      return reply.status(StatusCodes.OK).send({
        ok: true,
        data,
        // total: Number(affectedRow.total)
      });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          message: 'error',
          error
        });
    }
  });

  done();
} 
