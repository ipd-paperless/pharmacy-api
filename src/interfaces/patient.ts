import { UUID } from "crypto";
import { DateTime } from "luxon";

interface IPatient {
  id: UUID
  an: string
  hn: string
  cid?: string
  title?: string
  fname?: string
  lname?: string
  gender?: string
  nationality?: string
  citizenship?: string
  religion?: string
  dob?: string
  age?: string
  occupation?: string
  inscl?: string
  address?: string
  phone?: string
  is_pregnant?: boolean
  reference_person_name?: string
  reference_person_address?: string
  reference_person_phone?: string
  reference_person_relationship?: string
  insurance_id?: UUID
  admit_id?: UUID
  is_active?: boolean
  create_date?: DateTime
  create_by?: UUID
  modify_date?: DateTime
  modify_by?: UUID
}

interface IPatientRequest {
  an: string
  hn: string
  cid?: string
  title?: string
  fname?: string
  lname?: string
  gender?: string
  nationality?: string
  citizenship?: string
  religion?: string
  dob?: string
  age?: string
  occupation?: string
  inscl?: string
  address?: string
  phone?: string
  is_pregnant?: boolean
  reference_person_name?: string
  reference_person_address?: string
  reference_person_phone?: string
  reference_person_relationship?: string
  insurance_id?: UUID
  admit_id?: UUID
  is_active?: boolean
  create_by?: UUID
  modify_by?: UUID
}

interface IPatientResponse {
  id: UUID
  an: string
  hn: string
  cid?: string
  title?: string
  fname?: string
  lname?: string
  gender?: string
  nationality?: string
  citizenship?: string
  religion?: string
  dob?: string
  age?: string
  occupation?: string
  inscl?: string
  address?: string
  phone?: string
  is_pregnant?: boolean
  reference_person_name?: string
  reference_person_address?: string
  reference_person_phone?: string
  reference_person_relationship?: string
  insurance_id?: UUID
  admit_id?: UUID
}