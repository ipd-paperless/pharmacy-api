import { UUID } from "crypto";
import { DateTime } from "luxon";

interface IAdmit {
  id: UUID
  an: string
  hn: string
  vn: string
  admit_date: Date
  admit_time?: string
  department_id?: UUID  
  ward_id?: UUID
  bed_id?: UUID
  bed_number?: string
  doctor_id?: UUID
  pre_diag?: string
  insurance_id?: UUID
  inscl?: string
  inscl_name: string
  admit_by?: string
  is_active?: string
  create_date?: DateTime
  create_by?: UUID
  modify_date?: DateTime
  modify_by?: UUID
}

interface IAdmitRequest {
  an: string
  hn: string
  vn: string
  admit_date: Date
  admit_time?: string
  department_id?: UUID
  ward_id?: UUID
  bed_id?: UUID
  bed_number?: string
  doctor_id?: UUID
  pre_diag?: string
  insurance_id?: UUID
  inscl?: string
  inscl_name: string
  admit_by?: string
  is_active?: string
  create_date?: DateTime
  create_by?: UUID
  modify_date?: DateTime
  modify_by?: UUID
}

interface IAdmitResponse {
  id: UUID
  an: string
  hn: string
  vn: string
  admit_date: Date
  admit_time?: string
  department_id?: UUID  
  ward_id?: UUID
  bed_id?: UUID
  bed_number?: string
  doctor_id?: UUID
  pre_diag?: string
  insurance_id?: UUID
  inscl?: string
  inscl_name: string
  admit_by?: string
}