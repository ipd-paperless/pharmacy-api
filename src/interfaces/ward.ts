import { UUID } from "crypto";
import { DateTime } from "luxon";

interface IWard {
  id: UUID
  department_id: UUID
  code: string
  name: string
  icon_select?: string
  is_active?: boolean
  create_date?: DateTime
  create_by?: UUID
  modify_date?: DateTime
  modify_by?: UUID
}