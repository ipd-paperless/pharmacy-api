import { UUID } from "crypto";
import { Knex } from "knex";

export class WardModel {
  getHasBedID(db: Knex, bedIDs: any[]): any {
    throw new Error("Method not implemented.");
  }
  constructor() { }

  getByIDs(db: Knex, wardIDs: UUID[]) {
    return db('h_ward')
    .whereIn('id', wardIDs)
    .select(
      'id', 'name', 'code'
    )
  }

}