import { UUID } from 'crypto';
import { Knex } from 'knex';

export class PatientModel {

  constructor() { }

  getByANs(db: Knex, ans: any) {
    return db('patient')
      .where('is_active', true)
      .whereIn('an', ans)
      .select(
        'id', 'cid', 'an',
        'title', 'fname', 'lname', 'gender',
        'nationality', 'citizenship', 'religion',
        'dob', 'age', 'occupation',
        'inscl', 'address', 'phone', 'is_pregnant',
        'reference_person_name', 'reference_person_address',
        'reference_person_phone', 'reference_person_relationship',
        'insurance_id', 'inscl_name', 'blood_group'
      );
  }

}