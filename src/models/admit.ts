import { UUID } from 'crypto';
import knex, { Knex } from 'knex';

export class AdmitModel {

  constructor() { }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: object) {
    return db('admit')
      .returning('id')
      .insert(data)
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param id 
   * @returns 
   */
  update(db: Knex, data: object, id: UUID) {
    return db('admit')
      .returning('id')
      .where('id', id)
      .update(data);
  }

  /**
   * 
   * @param db 
   * @param id 
   * @returns 
   */
  delete(db: Knex, id: UUID) {
    return db('admit')
      .returning('id')
      .where('id', id)
      .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('admit')
      .returning('id')
      .insert(data)
      .onConflict('id')
      .merge();
  }

  /**
   * 
   * @param db 
   * @param id 
   * @returns 
   */
  getByID(db: Knex, id: UUID) {
    return db('admit')
      .where('id', id)
      .andWhere('is_active', true)
      .select(
        'id', 'an', 'hn', 'vn', 'admit_date', 'admit_time',
        'department_id', 'ward_id', 'bed_id', 'bed_number',
        'doctor_id', 'pre_diag',
        'insurance_id', 'inscl', 'inscl_name'
      )
      .first();
  }

  getAll(db: Knex, search: any, limit: number, offset: number) {
    let sql = db('admit as a')
      .innerJoin('patient as p', 'a.an', 'p.an')
      .where('a.is_active', true)
      .select(
        'a.id', 'a.an', 'a.hn', 'a.vn',
        'a.admit_date', 'a.admit_time', 
        'a.department_id', 'a.ward_id', 'a.bed_id', 'a.bed_number',
        'a.doctor_id', 'a.pre_diag', 'a.inscl', 'a.inscl_name',
        'a.insurance_id'
      )

    if (search) {
      let query = `${search}%`
      sql.where(builder => {
        builder.whereRaw('a.hn like ?', [query])
          .orWhereRaw('a.an like ?', [query])
          .orWhereRaw('p.cid like ?', [query])
          .orWhereRaw('LOWER(p.fname) like LOWER(?)', [query])
          .orWhereRaw('LOWER(p.lname) like LOWER(?)', [query])
      })
    }

    return sql
      .limit(limit)
      .offset(offset)
  }

  getTotal(db: Knex, search: any) {
    let sql = db('admit as a')
      .innerJoin('patient as p', 'a.an', 'p.an')
      .where('a.is_active', true)

    if (search) {
      let query = `${search}%`
      sql.where(builder => {
        builder.whereRaw('a.hn like ?', [query])
          .orWhereRaw('a.an like ?', [query])
          .orWhereRaw('p.cid like ?', [query])
          .orWhereRaw('LOWER(p.fname) like LOWER(?)', [query])
          .orWhereRaw('LOWER(p.lname) like LOWER(?)', [query])
      })
    }

    return sql
      .count({ total: '*' })
      .first();
  }
}