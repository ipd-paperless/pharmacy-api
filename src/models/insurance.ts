import { UUID } from "crypto";
import { Knex } from "knex";

export class InsuranceModel {
  constructor() { }

  getByIDs(db: Knex, insuranceIDs: UUID[]) {
    return db('h_insurance')
      .whereIn('id', insuranceIDs)
      .select(
        'id', 'name', 'code'
      )
  }

}