import { UUID } from "crypto";
import { Knex } from "knex";

export class BedModel {
  constructor() { }

  getByIDs(db: Knex, bedIDs: UUID[]) {
    return db('bed')
    .whereIn('id', bedIDs)
    .select(
      'id', 'name'
    )
  }

}