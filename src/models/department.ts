import { UUID } from "crypto";
import { Knex } from "knex";

export class DepartmentModel {
  constructor() { }

  getByIDs(db: Knex, departmentIDs: UUID[]) {
    return db('h_department')
    .whereIn('id', departmentIDs)
    .select(
      'id', 'name', 'code'
    )
  }

}