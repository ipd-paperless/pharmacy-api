import { UUID } from 'crypto';
import knex, { Knex } from 'knex';

export class DoctorModel {
  constructor() { }

  /**
   * 
   * @param db 
   * @param doctorID 
   * @returns 
   */
  getByUserID(db: Knex, userIDs: UUID[]) {
    return db('profile')
      .whereIn('user_id', userIDs)
      .select(
        'id', 'user_id',
        'title', 'fname', 'lname',
        'license_no', 'dob', 'sex'
      )
  }

}